libapache2-mod-bw (0.92-13) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 14:47:59 +0100

libapache2-mod-bw (0.92-12) unstable; urgency=medium

  * QA upload.
  * Orphan the package.
  * Refresh packaging:
    + Bump debhelper compat to v13.
    + Bump Standards-Version to 4.6.0.
    + Update homepage information.
  * debian/upstream/metadata: Add metadata file.
  * debian/watch: Monitor upstream release on GitHub.

 -- Boyuan Yang <byang@debian.org>  Wed, 25 Aug 2021 17:24:06 -0400

libapache2-mod-bw (0.92-11) unstable; urgency=medium

  * Bump up Standards-Version (to 3.9.6)

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Sat, 25 Oct 2014 16:50:49 +0400

libapache2-mod-bw (0.92-10) unstable; urgency=low

  * Fix lintian error: vcs-field-not-canonical
  * Bump up Standards-Version (to 3.9.5)
  * Add --no-silent to LTFLAGS

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Wed, 04 Dec 2013 01:42:51 +0400

libapache2-mod-bw (0.92-9) unstable; urgency=low

  * Repackage from experimental to sid (Closes: #709469)

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Sun, 16 Jun 2013 15:05:54 +0400

libapache2-mod-bw (0.92-8) experimental; urgency=low

  * Drop workaround for #666875

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Wed, 29 May 2013 00:11:47 +0400

libapache2-mod-bw (0.92-7) unstable; urgency=low

  * Drop DMUA, fix lintian warning
  * Bump up Standards-Version (to 3.9.4)

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Thu, 27 Dec 2012 14:14:27 +0400

libapache2-mod-bw (0.92-6) unstable; urgency=low

  * Fix FTBS on armel: add -D_LARGEFILE64_SOURCE to CFLAGS

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Fri, 15 Jun 2012 20:39:57 +0400

libapache2-mod-bw (0.92-5) unstable; urgency=low

  * Pass hardening CFLAGS/CPPFLAGS to apxs2
  * Add VCS-* fields
  * Override lintian W: hardening-no-fortify-functions

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Fri, 15 Jun 2012 17:46:24 +0400

libapache2-mod-bw (0.92-4) unstable; urgency=low

  * Reformat debian/copyright according to accepted DEP5 spec
  * Bump up Standards-Version to 3.9.3 (no changes)
  * Change dh compat to 9, enable hardening support

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Fri, 01 Jun 2012 15:42:33 +0400

libapache2-mod-bw (0.92-3) experimental; urgency=low

  * Reformat debian/copyright according to accepted DEP5 spec
  * Bump up Standards-Version to 3.9.3 (no changes)
  * Update Build-Depends and Depends for Apache 2.4
  * Switch to dh_apache2
  * Override dh_fixperms for #666875
  * Add Apache 2.4 compatibility patch (Closes: #666827)

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Sat, 07 Apr 2012 18:45:05 +0400

libapache2-mod-bw (0.92-2) unstable; urgency=low

  * Use tiny debian/rules, bump up debhelper compat level
  * Update maintainer scripts
  * Removed debian/dirs

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Mon, 21 Mar 2011 01:16:36 +0300

libapache2-mod-bw (0.92-1) unstable; urgency=low

  * update debhelper deps for dh_prep
  * replace dh_clean -k -> dh_prep
  * Specify upstream changelog (avoid upgrade compat version for this)
  * Register documentation in doc-base
  * New Upstream Version
  * Bump up Standards-Version (to 3.9.1)
  * Added "DM-Upload-Allowed: yes" control field

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Fri, 04 Mar 2011 14:44:58 +0300

libapache2-mod-bw (0.91-1) unstable; urgency=low

  * New Upstream Version (Closes: #588330).
  * Update format of debian/copyright, copyright years.
  * Switch to source format "3.0 (quilt)".
  * Bump Standards-Version to 3.8.4.

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Mon, 31 May 2010 11:07:17 +0400

libapache2-mod-bw (0.8-2) unstable; urgency=low

  * Change package Section to httpd and Priority to extra.
  * Bump up Standards-Version to 3.8.1.

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Wed, 20 May 2009 14:39:25 +0400

libapache2-mod-bw (0.8-1) unstable; urgency=low

  * Initial release (Closes: #481909)
  * Point to the common-licenses/Apache-2.0
  * Rewrite the description
  * Update the Standards-Version to 3.8.0
  * Add watch and tune the description

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Fri, 20 Jun 2008 23:53:27 +0400
